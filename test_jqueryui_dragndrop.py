from selenium import webdriver
from selenium.webdriver import ActionChains

driver = webdriver.Chrome()
driver.maximize_window()
driver.get("https://jqueryui.com/droppable/")
driver.switch_to.frame(0)

action_chain = ActionChains(driver)
source = driver.find_element_by_id("draggable")
target = driver.find_element_by_id("droppable")
action_chain.drag_and_drop(source, target).perform()

driver.close()
