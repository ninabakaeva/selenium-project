from selenium import webdriver
import time

driver = webdriver.Chrome()
driver.get('https://en.wikipedia.org/wiki/Main_Page')

search_field = driver.find_element_by_id("searchInput")
search_field.clear()
search_field.send_keys("new zealand")

search_button = driver.find_element_by_id("searchButton")
search_field.submit()
time.sleep(1)

assert driver.title == 'New Zealand - Wikipedia'

driver.close()