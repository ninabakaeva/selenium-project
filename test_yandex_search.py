from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait


def test_yandex_search():
    driver = WebDriver(executable_path='C://Users//vovoc//Documents//selenium//chromedriver_win32//chromedriver.exe')
    driver.get('https://ya.ru')
    search_input = driver.find_element_by_xpath('//input[@id="text"]')
    search_button = driver.find_element_by_xpath('//div[@class="search2__button"]//button[@type="submit"]')
    search_input.send_keys('market.yandex.ru')
    search_button.click()

    driver.implicitly_wait(5)

    search_results = driver.find_element_by_xpath('//li[@class="serp-item"]')
    link = search_results[10].find_element_by_xpath('.//h2/a')
    link.click()

    driver.switch_to.window(driver.window_handles[1])

    assert driver.title == 'Яндекс.Маркет — сервис для сравнения и выбора товаров из проверенных интернет-магазинов'



