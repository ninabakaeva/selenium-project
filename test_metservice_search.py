from selenium import webdriver
import time

driver = webdriver.Chrome()
driver.get('https://www.metservice.com/')
search_button = driver.find_element_by_class_name("SearchBar-actions-find")
search_button.click()

search_input = driver.find_element_by_class_name("SearchOverlay-bar-input-field")
search_input.send_keys('Wellington')

time.sleep(2)
search_result = driver.find_element_by_xpath("//a[@href='/towns-cities/locations/wellington/7-days']")
search_result.click()

assert driver.title == 'MetService - Te Ratonga Tirorangi'

driver.close()